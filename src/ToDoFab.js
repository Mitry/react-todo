import React from 'react';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

const ToDoFab = (props) => {

    const {
        defaultItem,
        openItemDialog
    } = props;
    return (
        <Fab color="secondary" style={{ fontSize: 60, position: 'fixed', bottom: '2rem', right: '2rem' }} onClick={() => openItemDialog(defaultItem)} >
            <AddIcon />
        </Fab>);
};

export default ToDoFab;