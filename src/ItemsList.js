import React from 'react';
import List from '@material-ui/core/List';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import TrendingDownIcon from '@material-ui/icons/TrendingDown';
import RemoveIcon from '@material-ui/icons/Remove';
import * as consts from './Consts';
import Item from './Item';

const ItemsList = (props) => {
    const {
        filteredItems,
        updateStatus,
        openPriorityDialog,
        openItemDialog,
        destroy
    } = props;

    return (<List>
        {filteredItems.map(item => {

            const labelId = `checkbox-list-label-${item.id}`,
                isChecked = item.status_id === consts.STATUS_DONE;

            let itemTextStyles = {},
                priorityIcon = <RemoveIcon />;

            if (isChecked) {
                itemTextStyles = { textDecorationLine: 'line-through' };
            }

            if (item.priority_id === consts.HIGH_PRIORITY) {
                priorityIcon = <TrendingUpIcon />
            } else if (item.priority_id === consts.LOW_PRIORITY) {
                priorityIcon = <TrendingDownIcon />
            }

            return (
                <Item
                    key={'item-' + item.id}
                    labelId={labelId}
                    item={item}
                    isChecked={isChecked}
                    itemTextStyles={itemTextStyles}
                    priorityIcon={priorityIcon}

                    updateStatus={updateStatus}
                    openPriorityDialog={openPriorityDialog}
                    openItemDialog={openItemDialog}
                    destroy={destroy}
                />
            );
        })}
    </List>);
};

export default ItemsList;