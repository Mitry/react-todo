import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';


const PrioritiesDialog = (props) => {

    const {
        isPriorityDialogOpen,
        selectedItem,
        priorities,
        items,
        classes,
        errorMsg,
        updatePriority,
        closeDialogs
    } = props;

    return (<Dialog
    
          

        open={isPriorityDialogOpen}>

        {errorMsg}

        <List   style={{ width: '80vh'}}>
            {priorities.map(priority => {
                return (
                    <ListItem
                        key={'priority-item-' + priority.id}
                        role={undefined} dense
                        button
                        selected={selectedItem.priority_id === priority.id}
                        onClick={() => updatePriority(priority.id)}
                    >
                        <ListItemIcon>
                            {priority.icon}
                        </ListItemIcon>
                        <ListItemText
                            disableTypography
                            primary={<Typography>{priority.label}</Typography>}
                        />
                    </ListItem>
                );
            })}
        </List>

        <DialogActions>
            <Button autoFocus onClick={() => closeDialogs(items)} color="primary">
                Close
        </Button>
        </DialogActions>
    </Dialog>);
};

export default PrioritiesDialog;