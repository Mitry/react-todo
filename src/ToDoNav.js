import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

const ToDoNav = (props) => {
    const {
        toggleLabel,
        priorities,
        typeOfSorting,
        sortItemsByPriority,
        toggleDoneItems
    } = props;

    return (
        <>
            <ButtonGroup color="primary" aria-label="text primary button group" style={{ justifyContent: 'center' }}>
                {priorities.map(priority => {
                    return (<Button key={'sort-priority-' + priority.id} startIcon={priority.icon} onClick={() => sortItemsByPriority(priority.id)} disabled={typeOfSorting === priority.id}>{priority.sortLabel}</Button>);
                })}
            </ButtonGroup>

            <ButtonGroup color="primary" aria-label="text primary button group" style={{ float: 'right' }}>
                <Button startIcon={toggleLabel.icon}  onClick={() => toggleDoneItems()}>{toggleLabel.text}</Button>
            </ButtonGroup>

        </>);

};

export default ToDoNav;