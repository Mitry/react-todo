import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Typography from '@material-ui/core/Typography';
import ClearIcon from '@material-ui/icons/Clear';

const Item = (props) => {
    const {
        item,
        labelId,
        isChecked,
        itemTextStyles,
        priorityIcon,
        openPriorityDialog,
        openItemDialog,
        destroy,
        updateStatus
    } = props;

    return (
        <ListItem key={item.id} dense button onClick={() => updateStatus(item, false)}>
            <ListItemIcon>
                <Checkbox
                    style={{ width: 60, height: 60 }}
                    icon={<CheckBoxOutlineBlankIcon style={{ fontSize: 40 }} />}
                    checkedIcon={<CheckBoxIcon style={{ fontSize: 40 }} />}
                    edge="start"
                    checked={isChecked}
                    tabIndex={-1}
                    color="default"
                    inputProps={{ 'aria-labelledby': labelId }}
                />
            </ListItemIcon>

            <ListItemText
                id={labelId}
                disableTypography
                primary={<Typography style={itemTextStyles}>{item.note}</Typography>}
            />

            <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="comments" onClick={() => openPriorityDialog(item)}>
                    {priorityIcon}
                </IconButton>
                <IconButton edge="end" aria-label="comments" onClick={() => openItemDialog(item)}>
                    <EditIcon />
                </IconButton>
                <IconButton edge="end" aria-label="comments" onClick={() => destroy(item)}>
                    <ClearIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>);
};

export default Item;