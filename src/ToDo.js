import React from 'react';
import axios from "axios/index";
import Button from '@material-ui/core/Button';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import TrendingDownIcon from '@material-ui/icons/TrendingDown';
import RemoveIcon from '@material-ui/icons/Remove';
import * as consts from './Consts';
import ToDoFab from './ToDoFab';
import ItemDialog from './ItemDialog';
import PrioritiesDialog from './PrioritiesDialog';
import ItemsList from './ItemsList';
import ToDoNav from './ToDoNav';
import Alert from '@material-ui/lab/Alert';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import LibraryAddCheckRoundedIcon from '@material-ui/icons/LibraryAddCheckRounded';
import RoundedCornerSharpIcon from '@material-ui/icons/RoundedCornerSharp';

class ToDo extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      items: [],
      errorMsg: null,
      generalErrorMsg: null,
      itemDialogCtaBtn: null,
      isItemDialogOpen: false,
      isPriorityDialogOpen: false,
      typeOfSorting: consts.MEDIUM_PRIORITY,
      isEachItemVisible: true,
      selectedItem: this.getDefaultItem(),
      priorities: [{ id: consts.HIGH_PRIORITY, name: 'high', label: 'High priority', sortLabel: 'High', icon: <TrendingUpIcon /> }, { id: consts.MEDIUM_PRIORITY, name: 'medium', label: 'Medium priority', sortLabel: 'Default', icon: <RemoveIcon /> }, { id: consts.LOW_PRIORITY, name: 'low', label: 'Low priority', sortLabel: 'Low', icon: <TrendingDownIcon /> }],
      classes: {
        root: {
          width: '100%',
          maxWidth: 360,
          flexGrow: 1,
        },
        paper: {
          width: '80%',
          maxHeight: 435,
        },
      }
    }
  }

  getDefaultItem = () => {
    return { user_id: consts.DEFAULT_PLACEHOLDER_USER_ID, note: '', status_id: consts.STATUS_ACTIVE, priority_id: consts.MEDIUM_PRIORITY };
  }

  toggleDoneItems = () => {
    const {
      isEachItemVisible
    } = this.state;

    this.setState({
      ...this.state,
      isEachItemVisible: !isEachItemVisible
    })
  }

  isUniqueNote = (selectedItem) => {
    const {
      items
    } = this.state,
      sanitizedNote = this.sanitizeStr(selectedItem.note);

    return !items.some(item => this.sanitizeStr(item.note) === sanitizedNote && item.id !== selectedItem.id);
  }

  sanitizeStr = (str) => {
    return str.toLowerCase().replace(/[^a-z0-9]/gi, '');
  }

  componentDidMount() {
    axios.get(consts.DEFAULT_PLACEHOLDER_DEV_LOCALHOST_URL + '/items?user_id=' + consts.DEFAULT_PLACEHOLDER_USER_ID).then(res => {
      this.setState({
        ...this.state,
        items: res.data
      });
    }).catch(res => {
      this.setState({
        ...this.state,
        generalErrorMsg: <Alert severity="error"> {consts.GENERIC_ERROR_MSG}</Alert>
      });
    });
  }

  openItemDialog = (item) => {
    let {
      selectedItem
    } = this.state;
    selectedItem = { ...item };
    const itemDialogCtaBtn = !!item.id ? <Button onClick={() => this.update(selectedItem)} color="primary">Edit</Button> : <Button onClick={() => this.create()} color="primary">Add</Button>;
    this.setState({
      ...this.state,
      generalErrorMsg: null,
      isItemDialogOpen: true,
      selectedItem: selectedItem,
      itemDialogCtaBtn: itemDialogCtaBtn
    })
  };

  openPriorityDialog = (item) => {
    this.setState({
      ...this.state,
      generalErrorMsg: null,
      isPriorityDialogOpen: true,
      selectedItem: item
    })
  };

  closeDialogs = (items) => {
    this.setState({
      ...this.state,
      errorMsg: null,
      items: items,
      isItemDialogOpen: false,
      isPriorityDialogOpen: false,
      selectedItem: this.getDefaultItem()
    });
  };

  create = () => {
    let {
      items,
      selectedItem
    } = this.state;

    if (selectedItem.note.length === 0) {
      this.setState({
        ...this.state,
        errorMsg: <Alert severity="error"> {consts.EMPTY_NOTE_ERROR_MSG}</Alert>
      });
    } else if (!this.isUniqueNote(selectedItem)) {
      this.setState({
        ...this.state,

        errorMsg: <Alert severity="error"> {consts.NOT_UNIQUE_NOTE_ERROR_MSG}</Alert>
      });
    } else {
      axios.post(consts.DEFAULT_PLACEHOLDER_DEV_LOCALHOST_URL + '/items',
        {
          item: {
            note: selectedItem.note,
            status_id: selectedItem.status_id,
            priority_id: selectedItem.priority_id,
            user_id: selectedItem.user_id
          }
        }).then((response) => {

          items.unshift(response.data);
          this.closeDialogs(items);
        }).catch((error) => {
          this.setState({
            ...this.state,
            errorMsg: <Alert severity="error"> {consts.GENERIC_ERROR_MSG}</Alert>
          });
        });
    }
  };

  setNote = (note) => {
    const {
      selectedItem
    } = this.state;
    selectedItem.note = note;

    this.setState({
      ...this.state,
      selectedItem: selectedItem
    });
  }

  sortItemsByPriority = (typeOfSorting) => {
    let {
      items
    } = this.state;

    switch (typeOfSorting) {
      case consts.HIGH_PRIORITY:
        items = items.sort((a, b) => a.priority_id - b.priority_id);
        break;
      case consts.LOW_PRIORITY:
        items = items.sort((a, b) => b.priority_id - a.priority_id);
        break;
      default:
        items = items.sort((a, b) => b.id - a.id);
        break;
    }
    this.setState({
      ...this.state,
      typeOfSorting: typeOfSorting,
      items: items
    });
  }

  destroy = (deletedItem) => {
    let {
      items
    } = this.state;

    axios.patch(consts.DEFAULT_PLACEHOLDER_DEV_LOCALHOST_URL + '/items/' + deletedItem.id,
      {
        item: {
          status_id: consts.STATUS_DELETED,
        }
      }).then((response) => {
        items = items.filter(item => item.id !== deletedItem.id);

        this.closeDialogs(items);
      }).catch((error) => {
        this.setState({
          ...this.state,
          generalErrorMsg: <Alert severity="error"> {consts.GENERIC_ERROR_MSG}</Alert>
        });
      });
  };

  update = (item) => {
    const {
      items
    } = this.state;

    if (!item.note || item.note.length === 0) {
      this.setState({
        ...this.state,

        errorMsg: <Alert severity="error"> {consts.EMPTY_NOTE_ERROR_MSG}</Alert>
      });
    } else if (!this.isUniqueNote(item)) {
      this.setState({
        ...this.state,

        errorMsg: <Alert severity="error"> {consts.NOT_UNIQUE_NOTE_ERROR_MSG}</Alert>
      });
    } else {
      axios.patch(consts.DEFAULT_PLACEHOLDER_DEV_LOCALHOST_URL + '/items/' + item.id,
        {
          item: {
            note: item.note,
            status_id: item.status_id,
            priority_id: item.priority_id,
          }
        }).then((response) => {
          for (let i = 0; i < items.length; i++) {
            if (items[i].id === item.id) {
              items[i].note = item.note;
              items[i].status_id = item.status_id;
              items[i].priority_id = item.priority_id;
              break;
            }
          }

          this.closeDialogs(items);
        }).catch((error) => {
          this.setState({
            ...this.state,
            errorMsg: <Alert severity="error"> {consts.GENERIC_ERROR_MSG}</Alert>
          });
        });
    }
  };

  updateStatus = (item) => {
    item.status_id = item.status_id === consts.STATUS_ACTIVE ? consts.STATUS_DONE : consts.STATUS_ACTIVE;
    this.update(item)
  };

  updatePriority = (priorityId) => {
    let {
      selectedItem
    } = this.state;

    selectedItem.priority_id = priorityId;
    this.update(selectedItem);
  };

  switchPriority = (priorityId) => {
    let {
      selectedItem
    } = this.state;

    selectedItem.priority_id = priorityId;
    this.setState({
      ...this.state,
      selectedItem: selectedItem
    });
  };

  render() {
    const {
      items,
      isItemDialogOpen,
      isPriorityDialogOpen,
      isEachItemVisible,
      typeOfSorting,
      selectedItem,
      priorities,
      classes,
      itemDialogCtaBtn,
      errorMsg,
      generalErrorMsg
    } = this.state;

    let filteredItems = items,
      toggleLabel = {icon: <RoundedCornerSharpIcon />, text: 'Hide Finished Items'};

    if (!isEachItemVisible) {
      filteredItems = items.filter(item => item.status_id === consts.STATUS_ACTIVE);
      toggleLabel = {icon: <LibraryAddCheckRoundedIcon />, text: 'Show Finished Items'};
    }

    return (
      <Container component="main" maxWidth="sm">
        <CssBaseline />
        <Box m={5} />
          <Grid item xs={12}>
            <Paper >
              <ToDoNav
                typeOfSorting={typeOfSorting}
                toggleLabel={toggleLabel}
                priorities={priorities}
                toggleDoneItems={this.toggleDoneItems}
                sortItemsByPriority={this.sortItemsByPriority} />
              <Box m={1} />
              {generalErrorMsg}
              <Box m={1} />
              <ItemsList
                filteredItems={filteredItems}
                classes={classes}
                updateStatus={this.updateStatus}
                openPriorityDialog={this.openPriorityDialog}
                openItemDialog={this.openItemDialog}
                destroy={this.destroy} />
              <PrioritiesDialog
                errorMsg={errorMsg}
                selectedItem={selectedItem}
                items={items}
                closeDialogs={this.closeDialogs}
                updatePriority={this.updatePriority}
                priorities={priorities}
                classes={classes}
                isPriorityDialogOpen={isPriorityDialogOpen} />
              <ItemDialog
                errorMsg={errorMsg}
                isItemDialogOpen={isItemDialogOpen}
                selectedItem={selectedItem}
                itemDialogCtaBtn={itemDialogCtaBtn}
                items={items} priorities={priorities}
                setNote={this.setNote}
                switchPriority={this.switchPriority}
                closeDialogs={this.closeDialogs} />
              <ToDoFab
                defaultItem={this.getDefaultItem()}
                openItemDialog={this.openItemDialog} />
            </Paper>
          </Grid>
      </Container>

    );
  }
}

export default ToDo;