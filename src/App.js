import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from "./Login";
import SignUp from "./SignUp";
import ToDo from './ToDo';


function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/" component={ToDo} />
        </div>
      </Router>
    </div>
  );
}

export default App;
