import React from 'react';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import ButtonGroup from '@material-ui/core/ButtonGroup';

const ItemDialog = (props) => {

    const {
        isItemDialogOpen,
        selectedItem,
        priorities,
        itemDialogCtaBtn,
        items,
        errorMsg,
        setNote,
        switchPriority,
        closeDialogs
    } = props;

    return (
        <Dialog

            open={isItemDialogOpen}>
            <DialogTitle id="simple-dialog-title">
                <ButtonGroup color="primary" aria-label="text primary button group" style={{ justifyContent: 'center' }}>
                    {priorities.map(priority => {
                        const isDisabled = priority.id === selectedItem.priority_id;
                        return (<Button key={'priority-' + priority.id} startIcon={priority.icon} onClick={() => switchPriority(priority.id)} disabled={isDisabled}> {priority.name} </Button>);
                    })}
                </ButtonGroup>
            </DialogTitle>

            {errorMsg}

            <TextField
                style={{ width: '80vh' }}
                id="outlined-multiline-static"
                label="Note"
                multiline
                rows="4"
                onChange={e => setNote(e.target.value)}
                value={selectedItem.note}
                variant="outlined" />

            <DialogActions>
                <Button onClick={() => closeDialogs(items)} color="primary">
                    Cancel
                    </Button>
                {itemDialogCtaBtn}
            </DialogActions>
        </Dialog>);
};

export default ItemDialog;